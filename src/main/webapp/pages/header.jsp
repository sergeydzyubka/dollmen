<%--
  Created by IntelliJ IDEA.
  User: sergeydzyubka
  Date: 12.04.15
  Time: 22:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="templatemo_top_section">
    <div class="templatemo_container">
        <div id="templatemo_header">
            <div id="templatemo_logo_area">
                <a href="/"><img src="/images/logo_horiz.png" height="100" width="244"></a>
                <%--<h2><a href="/" target="_parent">Dollmen</a></h2>--%>
            </div>

            <%--<div id="templatemo_top_right">--%>
            <%--<div id="templatemo_subscribe">--%>
            <%--<span class="subscribe_text">--%>
            <%--SUBSCRIBE--%>
            <%--</span>--%>
            <%--</div>--%>
            <div id="templatemo_social">
                <a href="https://www.facebook.com/dollmen.co" target="_blank">
                    <img src="/images/facebook.png" alt="facebook" />
                </a>

                <a href="https://www.youtube.com/channel/UCHVUpFIuudqji6DnMxjP2Aw" target="_blank">
                    <img src="/images/youtube.png" alt="youtube" />
                </a>

                <a href="https://vimeo.com/channels/908308" target="_blank">
                    <img src="/images/vimeo.png" alt="vimeo" />
                </a>

                <a href="https://www.flickr.com/photos/132716875@N07/" target="_blank">
                    <img src="/images/flickr.png" alt="flickr" />
                </a>
            </div>

            <!-- End Of Top Section -->