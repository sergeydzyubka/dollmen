<%--
  Created by IntelliJ IDEA.
  User: sergeydzyubka
  Date: 18.04.15
  Time: 21:36
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <title>Dashboard I Admin Panel</title>

    <link rel="stylesheet" href="/css/adminlayout.css" type="text/css" media="screen" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="/css/ie.css" type="text/css" media="screen" />
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="/js/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="/js/hideshow.js" type="text/javascript"></script>
    <script src="/js/jquery.tablesorter.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.equalHeight.js"></script>
    <script type="text/javascript">
        $(document).ready(function()
                {
                    $(".tablesorter").tablesorter();
                }
        );
        $(document).ready(function() {

            //When page loads...
            $(".tab_content").hide(); //Hide all content
            $("ul.tabs li:first").addClass("active").show(); //Activate first tab
            $(".tab_content:first").show(); //Show first tab content

            //On Click Event
            $("ul.tabs li").click(function() {

                $("ul.tabs li").removeClass("active"); //Remove any "active" class
                $(this).addClass("active"); //Add "active" class to selected tab
                $(".tab_content").hide(); //Hide all tab content

                var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
                $(activeTab).fadeIn(); //Fade in the active ID content
                return false;
            });

        });
    </script>
    <script type="text/javascript">
        $(function(){
            $('.column').equalHeight();
        });
    </script>

</head>


<body>

<jsp:include page="admintemplate.jsp"/>

<section id="main" class="column">

    <div class="clear"></div>
    </div>
    </article><!-- end of stats article -->

    <article class="module width_3_quarter">
        <header><h3 class="tabs_involved">Add file</h3>
        </header>
        <sf:form method="POST" commandName="addDownload" enctype="multipart/form-data">
            <sf:errors path="*" cssClass="errorblock" element="div"/>
            <tr>
                <sf:label for="file" path="file"><p class="youtube">Size must be less than 50 MB, file name must contain only digits and latin symbols!</p></sf:label>
                <p><sf:input path="file" type="file"/></p>
            </tr>
            <input type="submit" name="submit" value="Submit">
        </sf:form>
    </article><!-- end of content manager article -->

    <div class="clear"></div>

    <div class="spacer"></div>
    </div>
</section>

