<%--
  Created by IntelliJ IDEA.
  User: sergeydzyubka
  Date: 12.04.15
  Time: 22:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<header id="header">
    <hgroup>
        <h1 class="site_title"><a href="index.html">Dollmen admin panel</a></h1>
        <h2 class="section_title">Dashboard</h2>
    </hgroup>
</header> <!-- end of header bar -->

<section id="secondary_bar">
    <div class="user">
        <p>${pageContext.request.userPrincipal.name}</p>
        <a class="logout_user" href="/logout" title="Logout">Logout</a>
    </div>
</section><!-- end of secondary bar -->

<aside id="sidebar" class="column">
    <h3>Content</h3>
    <ul class="toggle">
        <li class="icn_new_article"><a href="/admin/project/add">New project</a></li>
        <li class="icn_edit_article"><a href="/admin/project/edit">Edit project</a></li>
        <li class="icn_edit_article"><a href="/admin/contacts">Edit contacts page</a></li>
        <li class="icn_edit_article"><a href="/admin/company">Edit company page</a></li>
        <li class="icn_new_article"><a href="/admin/partners/add">New partner</a></li>
        <li class="icn_edit_article"><a href="/admin/partners/edit">Edit partners</a></li>
        <li class="icn_new_article"><a href="/admin/downloads/add">New download</a></li>
        <li class="icn_edit_article"><a href="/admin/downloads/edit">Edit downloads</a></li>
        <li class="icn_edit_article"><a href="/admin/tickets">Edit tickets page</a></li>
        <%--<li class="icn_tags"><a href="#">Tags</a></li>--%>
    </ul>
    <h3>Users</h3>
    <ul class="toggle">
        <sec:authorize access="hasRole('ROLE_ADMIN')">
        <li class="icn_add_user"><a href="/admin/moderators/add">Add New User</a></li>
        <li class="icn_view_users"><a href="/admin/moderators/edit">View Users</a></li>
        </sec:authorize>
        <li class="icn_profile"><a href="/admin/profile">Your Profile</a></li>
    </ul>
    <%--<h3>Media</h3>--%>
    <%--<ul class="toggle">--%>
    <%--<li class="icn_folder"><a href="#">File Manager</a></li>--%>
    <%--<li class="icn_photo"><a href="#">Gallery</a></li>--%>
    <%--<li class="icn_audio"><a href="#">Audio</a></li>--%>
    <%--<li class="icn_video"><a href="#">Video</a></li>--%>
    <%--</ul>--%>
    <%--<h3>Admin</h3>--%>
    <%--<ul class="toggle">--%>
    <%--<li class="icn_settings"><a href="#">Options</a></li>--%>
    <%--<li class="icn_security"><a href="#">Security</a></li>--%>
    <%--<li class="icn_jump_back"><a href="#">Logout</a></li>--%>
    <%--</ul>--%>

    <footer>
        <hr />
        <p><strong>Copyright &copy; 2015 <br>Dollmen theater company</strong></p>
    </footer>
</aside><!-- end of sidebar -->