<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Dollmen</title>
    <meta name="keywords" content="Teal Com, Free CSS Template, Web Design" />
    <meta name="description" content="Teal Com - Free CSS Template, HTML CSS, Web Design Layout" />
    <link href="/css/templatemo_style.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/fullsize.css" media="screen" rel="stylesheet" type="text/css" />--%>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
</head>
<body>
<jsp:include page="header.jsp"/>
<div align="center" id="templatemo_menu">
    <ul>
        <li><a href="/projects" class="current">Projects</a></li>
        <li><a href="/company">Company</a></li>
        <li><a href="/partners">Partners</a></li>
        <li><a href="/contacts">Contacts</a></li>
        <li><a href="/downloads">Downloads</a></li>
        <li><a href="/tickets">Tickets</a></li>
    </ul>
</div>
</div>
</div><!-- End Of Container -->
</div>
<c:forEach var="project" items="${lists}" varStatus="status">
    <div class="templatemo_middle_section">
        <div class="templatemo_container">
            <div class="templatemo_content_area">
                <div class="titleproject"><a href="/projects/${project.id}"><h1>${project.title}</h1></a></div>
                <br>
                    <tr>
                        <div align="center"><a href="/projects/${project.id}"><img src="/images/${project.id}.jpg" width="100%" height="250"></a></div>
                    </tr>
            </div>
        </div>
    </div>
</c:forEach>

<%--Paging--%>
<div class="paginate wrapper">
    <ul>
<c:if test="${page != 1}">
    <li><a href="?page=${page - 1}">&lang;</a></li>
</c:if>

        <c:forEach begin="1" end="${numberPages}" var="i">
            <c:choose>
                <c:when test="${page eq i}">
                <li><a class="active">${i}</a></li>
                </c:when>
                <c:otherwise>
                <li><a href="?page=${i}">${i}</a></li>
                </c:otherwise>
            </c:choose>
        </c:forEach>

<%--For displaying Next link --%>
<c:if test="${page != numberPages}">
    <li><a href="?page=${page + 1}">&rang;</a></li>
</c:if>
    </ul>
</div>

<jsp:include page="footer.jsp"/>
</body>
</html>