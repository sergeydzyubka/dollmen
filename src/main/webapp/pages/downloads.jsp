<%--
  Created by IntelliJ IDEA.
  User: sergeydzyubka
  Date: 18.04.15
  Time: 1:26
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/FileSizeFormatter.tld" prefix="sz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Dollmen</title>
    <meta name="keywords" content="Teal Com, Free CSS Template, Web Design" />
    <meta name="description" content="Teal Com - Free CSS Template, HTML CSS, Web Design Layout" />
    <link href="/css/templatemo_style.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/fullsize.css" media="screen" rel="stylesheet" type="text/css" />--%>
</head>
<body>
<jsp:include page="header.jsp"/>
<div align="center" id="templatemo_menu">
    <ul>
        <li><a href="/projects">Projects</a></li>
        <li><a href="/company">Company</a></li>
        <li><a href="/partners">Partners</a></li>
        <li><a href="/contacts">Contacts</a></li>
        <li><a href="/downloads" class="current">Downloads</a></li>
        <li><a href="/tickets">Tickets</a></li>
    </ul>
</div>
</div>
</div><!-- End Of Container -->
</div>
<div class="templatemo_middle_section">
    <div class="templatemo_container">
        <div align="center" class="downloads_content_area">
            <table>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <c:forEach var="file" items="${files}" varStatus="loop">
                <tr><td><p align="center">${file.name}</td><td><p>${sz:fileSize(file.length())}</p></td><td><a href="downloads/${file.name}" target="_blank"><img align="middle" height="30" width="30" src="images/download.png"></a></td></tr>
                </c:forEach>
            </table>

                <%--<p>${file.name} <a href="downloads/${file.name}"><img align="middle" src="images/download.png"></a></p>--%>


        </div>
    </div>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
