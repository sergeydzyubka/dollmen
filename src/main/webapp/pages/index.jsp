<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="shortcut icon" href="/pages/favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Dollmen</title>
    <meta name="keywords" content="Teal Com, Free CSS Template, Web Design" />
    <meta name="description" content="Teal Com - Free CSS Template, HTML CSS, Web Design Layout" />
    <link href="/css/templatemo_style.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/fullsize.css" media="screen" rel="stylesheet" type="text/css" />--%>

</head>
<body>
<div id="templatemo_top_section">
    <div class="templatemo_container">
        <div id="templatemo_header">
            <div id="templatemo_logo_area">
                <h2><a href="/" target="_parent">Test logo</a></h2>
                <p>site development</p>
            </div>

            <div id="templatemo_top_right">
                <div id="templatemo_subscribe">
    	            	<span class="subscribe_text">
        	            	SUBSCRIBE
            	        </span>
                </div>
                <div id="templatemo_social">
                    <a href="#">
                        <img src="/images/templatemo_rss.png" alt="RSS" />
                    </a>

                    <a href="#">
                        <img src="/images/templatemo_delicious.png" alt="Delicious" />
                    </a>

                    <a href="#">
                        <img src="/images/templatemo_twitter.png" alt="Twitter" />
                    </a>
                </div>
            </div>

            <div id="templatemo_menu">
                <ul>
                    <li><a href="/projects/list" class="current">Projects</a></li>
                    <li><a href="#" target="_parent">About us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Our team</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </div>
        </div>
    </div><!-- End Of Container -->
</div><!-- End Of Top Section -->

<div class="templatemo_middle_section">
    <div class="templatemo_container">
        <div class="templatemo_content_area">

            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="leftcol"><img src="images/iphone.jpg"
                                             width="450" height="250" alt="Вы не поверите, но это ёжик"></td>
                    <td valign="top"> <p>Впервые iPhone был анонсирован Стивом Джобсом на конференции MacWorld Expo 9 января 2007 года.
                        В продажу поступил 29 июня 2007 года вместе с iPhone OS и быстро завоевал существенную часть рынка смартфонов в США.
                        Популярность iPhone OS поддержал вышедший в продажу в сентябре того же года iPod touch, обладавший, однако, заметно урезанной
                        функциональностью по сравнению с iPhone (изначально поставлялся с лишь 11 приложениями, против 17 (с учетом iTunes Wi-Fi Music Store)
                        на iPhone, в дальнейшем прошивки, расширяющие функциональность проигрывателя, продавались за $19.99 (до середины 2008 года), при этом
                        аналогичные апгрейды для iPhone были бесплатны).</p></td>
                </tr>
            </table>
            <%--<div class="cleaner"></div>--%>
        </div>
    </div>
</div>


<div class="templatemo_middle_section">
    <div class="templatemo_container">
        <div class="templatemo_content_area">

            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="leftcol"><img src="images/watch.jpg"
                                             width="450" height="250" alt="Вы не поверите, но это ёжик"></td>
                    <td valign="top"> <p>Умные часы (англ. smartwatch) — компьютеризированные наручные часы с расширенной функциональностью
                        (кроме стандартного слежения за временем), часто сравнимой с коммуникаторами. Первые модели выполняли простые задачи, например,
                        выступали в роли калькулятора, переводчика или игрового устройства. Современные умные часы — это носимые компьютеры. Многие модели
                        поддерживают сторонние приложения и управляются мобильными операционными системами, могут выступать в качестве мобильных медиа-плееров.
                        С помощью некоторых моделей можно принимать телефонные звонки и отвечать на SMS, и электронную почту. Некоторые умные часы работают
                        только в паре со смартфоном и выступают в роли вспомогательного экрана, который оповещает владельца о поступлении новых уведомлений
                        (например, сообщений в социальных сетях, звонков и напоминаний из календаря).</p></td>
                </tr>
            </table>
            <%--<div class="cleaner"></div>--%>
        </div>
    </div>
</div>

<div id="templatemo_footer">
    Copyright © 2015 <a href="#">Power development</a> |
    Designed by <a href="#" target="_parent">DzyuLeg - Dzyuba i Oleg</a>
</div>
</body>
</html>
