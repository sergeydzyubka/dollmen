<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Dollmen</title>
    <meta name="keywords" content="Teal Com, Free CSS Template, Web Design" />
    <meta name="description" content="Teal Com - Free CSS Template, HTML CSS, Web Design Layout" />
    <link href="/css/templatemo_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/youtube.js"></script>
    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>--%>
    <script type="text/javascript" src="/js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="/js/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/jquery.fancybox-1.3.4.css" media="screen" />
    <script type="text/javascript">
        $(document).ready(function() {
            $("a[rel=example_group]").fancybox({
                'transitionIn': 'none',
                'transitionOut': 'none',
                'titlePosition': 'over',
                'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
                    return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
                }
            });
        });
//        function bindImg(idElement){
//            $(document).ready(function() {
//                /*
//                 *   Examples - images
//                 */
//
//                $(idElement).fancybox();
//            });
//        }
//
//        bindImg("exempale1");
//        bindImg("exempale2");
//        bindImg("exempale3");
    </script>
    <%--<link href="css/fullsize.css" media="screen" rel="stylesheet" type="text/css" />--%>

</head>
<body>
<jsp:include page="header.jsp"/>
<div align="center" id="templatemo_menu">
    <ul>
        <li><a href="/projects" class="current">Projects</a></li>
        <li><a href="/company">Company</a></li>
        <li><a href="/partners">Partners</a></li>
        <li><a href="/contacts">Contacts</a></li>
        <li><a href="/downloads">Downloads</a></li>
        <li><a href="#">Tickets</a></li>
    </ul>
</div>
</div>
</div><!-- End Of Container -->
</div>

<div class="templatemo_middle_section">
    <div class="templatemo_container">
        <div class="templatemo_content_area">
            <div class="titleproject">${project.title}</div>
            <c:if test="${not empty project.video}">
                <br>
                <div align="center"><iframe id="player" type="text/html" width="640" height="390"
                                            src="http://www.youtube.com/embed/${project.video}" allowfullscreen="allowfullscreen"
                                            frameborder="0"></iframe></div>
            </c:if>
            <br>
            <tr>
                <div align="center"><p>${project.description}</p></div>
            </tr>

            <div class="albumblock" align="center">

            <c:forEach var="file" items="${albumList}" varStatus="loop">
                <c:if test="${not empty file}">
                <a rel="example_group" href="${file}"><img alt="example1" src="${file}"></a>
                </c:if>
            </c:forEach>

            </div>
        </div>
    </div>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>