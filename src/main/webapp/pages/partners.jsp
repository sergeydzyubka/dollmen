<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Dollmen</title>
  <meta name="keywords" content="Teal Com, Free CSS Template, Web Design" />
  <meta name="description" content="Teal Com - Free CSS Template, HTML CSS, Web Design Layout" />
  <link href="/css/templatemo_style.css" rel="stylesheet" type="text/css" />
  <%--<link href="css/fullsize.css" media="screen" rel="stylesheet" type="text/css" />--%>

</head>
<body>
<jsp:include page="header.jsp"/>
<div align="center" id="templatemo_menu">
    <ul>
        <li><a href="/projects">Projects</a></li>
        <li><a href="/company">Company</a></li>
        <li><a href="/partners" class="current">Partners</a></li>
        <li><a href="/contacts">Contacts</a></li>
        <li><a href="/downloads">Downloads</a></li>
        <li><a href="/tickets">Tickets</a></li>
    </ul>
</div>
</div>
</div><!-- End Of Container -->
</div>
  <div class="templatemo_middle_section">
    <div class="templatemo_container">
      <div class="partners_content_area">
        <c:forEach var="partner" items="${lists}" varStatus="loop">
          <br>
        <tr>
          <a href="${partner.value.url}" target="_blank"><img src="/images/partner/${partner.value.id}.jpg" width="50px" height="50px" class="leftimg"></a>
             <p>${partner.value.title}</p>
        </tr>
          <br>
        </c:forEach>
      </div>
    </div>
  </div>
<jsp:include page="footer.jsp"/>
</body>
</html>