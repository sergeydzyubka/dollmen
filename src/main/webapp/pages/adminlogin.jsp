<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>

  <meta charset="UTF-8">

  <title>Dollmen</title>

  <link rel="stylesheet" href="/css/style.css" media="screen" type="text/css" />

</head>

<body>

<div class="login-card">
  <h1>Log-in</h1><br>
  <c:url value="/j_spring_security_check" var="loginUrl" />
  <form id="id-form" action="${loginUrl}" method="post">
    <input type="text" name="j_username" placeholder="Username">
    <input type="password" name="j_password" placeholder="Password">
    <input type="submit" name="login" class="login login-submit" value="login">
  </form>

  <%--<div class="login-help">--%>
    <%--<a href="#">Register</a> • <a href="#">Forgot Password</a>--%>
  <%--</div>--%>
</div>
</body>

</html>