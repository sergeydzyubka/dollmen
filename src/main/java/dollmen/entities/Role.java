package dollmen.entities;

/**
 * Created by sergeydzyubka on 28.03.15.
 */
public enum Role
{
    ROLE_MODERATOR("ROLE_MODERATOR"),ROLE_ADMIN("ROLE_ADMIN");

    @Override
    public String toString()
    {
        return this.role;
    }

    private Role(String role)
    {
        this.role = role;
    }
    private final String role;
}
