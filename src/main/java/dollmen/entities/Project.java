package dollmen.entities;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by sergeydzyubka on 21.02.15.
 */
@Entity
@Table(name="projects")
public class Project implements java.io.Serializable, Cloneable, Comparable<Project>
{
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private Integer id;
    @Column(name="title")
    private String title;
    @Column(name="description")
    private String description;
    @Column(name="photos")
    private String photos;
    @Column(name="video")
    private String video;
    @ManyToOne(fetch = FetchType.EAGER)
    @org.hibernate.annotations.Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(name="author", nullable = false)
    private Account author;

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Project() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }


    @Override
    public int compareTo(Project o) {
        int compareId = o.getId();

        return compareId - this.id;

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + (photos != null ? photos.hashCode() : 0);
        result = 31 * result + video.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Project)) return false;

        Project project = (Project) o;

        if (!description.equals(project.description)) return false;
        if (!id.equals(project.id)) return false;
        if (photos != null ? !photos.equals(project.photos) : project.photos != null) return false;
        if (!title.equals(project.title)) return false;
        if (!video.equals(project.video)) return false;

        return true;
    }
}
