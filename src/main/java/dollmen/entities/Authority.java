package dollmen.entities;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Created by sergeydzyubka on 29.03.15.
 */
@Entity
@Table(name="authorities")
public class Authority implements GrantedAuthority
{
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private Integer id;

    @Column (name="role")
    private String role;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user", nullable = false)
    private Account account;

    public Authority()
    {}

    public Authority(String role, Account account) {
        this.role = role;
        this.account = account;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getRole()
    {
        return role;
    }

    public void setRole(Role role)
    {
        this.role = role.toString();
    }

    public Account getAccount()
    {
        return account;
    }

    public void setAccount(Account account)
    {
        this.account = account;
    }

    @Override
    public String getAuthority() {
        return role;
    }
}
