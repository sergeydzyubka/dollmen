package dollmen.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by sergeydzyubka on 10.04.15.
 */
@Entity
@Table(name = "partners")
public class Partner implements java.io.Serializable {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private Integer id;
    @Column(name = "title")
    private String title;
    @Column(name="url")
    private String url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Partner() {

    }

    public Partner(String title, String url) {

        this.title = title;
        this.url = url;
    }
}
