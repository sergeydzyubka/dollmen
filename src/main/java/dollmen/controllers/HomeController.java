package dollmen.controllers;

import dollmen.DAO.ProjectDAO;
import dollmen.entities.Project;
import dollmen.forms.ChangeCompanyForm;
import dollmen.forms.ChangeContactsForm;
import dollmen.forms.ChangeTicketsForm;
import dollmen.service.ProjectService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

@Controller
@RequestMapping("/")
public class HomeController {
    @Autowired
    ProjectDAO projectDAO;

    @Autowired
    ProjectService projectService;

    @Autowired
    private HashMap projectMap;

    @Autowired
    private HashMap partnersMap;

    @Autowired
    private ChangeContactsForm contacts;

    @Autowired
    private ChangeCompanyForm company;

    @Autowired
    private ChangeTicketsForm tickets;

    @Autowired
    private ArrayList projectsList;

    @Autowired
    private ServletContext servletContext;

    @RequestMapping(value = "/tickets", method = RequestMethod.GET)
    public ModelAndView ticketsPage() {
        if (tickets.getText() == null)
            tickets.setText(tickets.read());
        ModelAndView model = new ModelAndView();
        model.addObject("tickets", tickets.getText());
        model.setViewName("tickets");
        return model;
    }

    @RequestMapping(value = "/downloads", method = RequestMethod.GET)
    public ModelAndView getDownloadsPage ()
    {
        ModelAndView model = new ModelAndView();
        String contextPath = servletContext.getRealPath("/pages/");
        File[] files = new File(contextPath + "/downloads").listFiles();
        model.addObject("files",files);
        return model;
    }

    @RequestMapping(value = "/downloads/{filename}/{type}", method = RequestMethod.GET)
    public  @ResponseBody
    void showDownloads(@PathVariable String filename,@PathVariable String type,HttpServletRequest request,
                                            HttpServletResponse response) throws IOException {

        ServletContext context = request.getServletContext();
        String path = context.getRealPath("/pages/")+"/downloads/"+ filename +type;
        File downloadFile = new File(path);
        FileInputStream inputStream = null;
        OutputStream outStream = null;

        try {
            inputStream = new FileInputStream(downloadFile);

            response.setContentLength((int) downloadFile.length());
            response.setContentType(context.getMimeType(path));

            // response header
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"",downloadFile.getName());
            response.setHeader(headerKey, headerValue);

            // Write response
            outStream = response.getOutputStream();
            IOUtils.copy(inputStream, outStream);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != inputStream)
                    inputStream.close();
                if (null != inputStream)
                    outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @RequestMapping(value = "projects/{id}", method = RequestMethod.GET)
    public String getProject(@PathVariable Integer id, Model model) {
        Project p = (Project) projectMap.get(id);
        model.addAttribute("project", p);
        String contextPath = servletContext.getRealPath("/pages/");
        if (new File(contextPath +"/images/albums/"+id).exists()) {
            model.addAttribute("albumList", projectService.getAlbumForSite(id));
        }
        return "project";
    }

    @RequestMapping(value = {"/", "projects"}, method = {RequestMethod.GET, RequestMethod.HEAD})
    public ModelAndView showProjects(@RequestParam (value = "page", required = false, defaultValue = "1") int page) {
        ModelAndView model = new ModelAndView();
        int numberPages = (int) Math.ceil(projectMap.size() * 1.0 / 5);
        model.addObject("lists", projectDAO.getProjectsPerPage(page));
        model.addObject("count", projectMap.size());
        model.addObject("numberPages", numberPages);
        model.addObject("page", page);
        model.setViewName("projectslist");
        return model;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView loginPage() {
        ModelAndView model = new ModelAndView();
        model.setViewName("adminlogin");
        return model;
    }

    @RequestMapping(value = "/contacts", method = RequestMethod.GET)
    public ModelAndView contactsPage() {
        if (contacts.getText() == null)
            contacts.setText(contacts.read());
        ModelAndView model = new ModelAndView();
        model.addObject("contacts", contacts.getText());
        model.setViewName("contacts");
        return model;
    }
    @RequestMapping(value = "/company", method = RequestMethod.GET)
    public ModelAndView companyPage()
    {
        if (company.getText() == null)
            company.setText(company.read());
        ModelAndView model = new ModelAndView();
        model.addObject("company", company.getText());
        model.setViewName("company");
        return model;
    }

    @RequestMapping(value = "/partners", method = RequestMethod.GET)
    public ModelAndView showPartners() {
        ModelAndView model = new ModelAndView();
        model.addObject("lists", partnersMap);
        model.setViewName("partners");
        return model;
    }
}