package dollmen.controllers;

import dollmen.common.ImageUploadException;
import dollmen.entities.Account;
import dollmen.entities.Partner;
import dollmen.entities.Project;
import dollmen.forms.*;
import dollmen.service.AccountService;
import dollmen.service.PartnerService;
import dollmen.service.ProjectService;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController{

    @Autowired
    AccountService accountService;

    @Autowired
    PartnerService partnerService;

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ShaPasswordEncoder shaPasswordEncoder;

    @Autowired
    private HashMap projectMap;

    @Autowired
    private HashMap accountMap;

    @Autowired
    private HashMap partnersMap;

    @Autowired
    @Lazy
    private Account currentUser;

    @Autowired
    @Lazy
    private CommonsMultipartResolver commonsMultipartResolver;

    @Autowired
    @Lazy
    private ChangeContactsForm contactsForm;

    @Autowired
    @Lazy
    private ChangeTicketsForm ticketsForm;

    @Autowired
    @Lazy
    private ChangeCompanyForm companyForm;

    static final Logger logger = Logger.getLogger(AdminController.class);

    @RequestMapping (value = "/project/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteProject(@PathVariable Integer id)
    {
        ModelAndView modelAndView = new ModelAndView();
        projectService.remove((Project) projectMap.get(id));
        modelAndView.setViewName("redirect:/admin/project/edit");
        return modelAndView;
    }

    @RequestMapping(value ="/downloads/add", method = RequestMethod.POST)
    public ModelAndView addDownload(@Valid @ModelAttribute ("addDownload") AddDownload addDownload,
                                    @RequestParam(value="file", required=false) MultipartFile file, BindingResult errors)
    {
        ModelAndView modelAndView = new ModelAndView();
        String filename = file.getOriginalFilename().replaceAll(" ","_");
        if (errors.hasErrors())
            modelAndView.setViewName("admin/adddownload");
        if(file.getSize()==0 || file.getSize()>52428800)
        {
            errors.rejectValue("file", "required.fileUpload", "File cannot be empty and it's size must be less than 50 MB!");
            modelAndView.setViewName("admin/adddownload");
        }
        else
        {
            accountService.addDownload(filename, file);
            modelAndView.setViewName("admin/adddownload");
            logger.info(currentUser+" added a new download"+filename);
        }
        return modelAndView;
    }

    @RequestMapping(value ="/downloads/add", method = RequestMethod.GET)
    public ModelAndView showAddDownloadPage ()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("addDownload", new AddDownload());
        modelAndView.setViewName("admin/adddownload");
        return modelAndView;
    }

    @RequestMapping(value = "/downloads/edit", method = RequestMethod.GET)
    public ModelAndView showDownloadsPage ()
    {
        ModelAndView modelAndView = new ModelAndView();
        String contextPath = servletContext.getRealPath("/pages/");
        File[] files = new File(contextPath + "/downloads").listFiles();
        modelAndView.addObject("files",files);
        modelAndView.setViewName("admin/editdownloads");
        return modelAndView;
    }

    @RequestMapping(value = "/downloads/delete/{name}.{type}", method = RequestMethod.GET)
    public ModelAndView deleteDownload (@PathVariable String name, @PathVariable String type)
    {
        name=name+"."+type;
        accountService.deleteDownload(name);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/admin/downloads/edit");
        logger.info(currentUser.getUsername()+"removed file"+name+type+" from downloads");
        return modelAndView;
    }

    @RequestMapping(value ="/project/edit", method = RequestMethod.GET)
    public ModelAndView showEditPage() {
        ModelAndView model = new ModelAndView();
        model.addObject("lists", projectMap);
        model.setViewName("admin/listprojects");
        return model;
    }

    @RequestMapping(value ="/project/edit/{id}", method = RequestMethod.GET)
    public String showEditPage(@PathVariable Integer id, Model model) {
        Project p = (Project) projectMap.get(id);
        model.addAttribute("project", p);
        List <DeleteImageForm> deleteImageForms = new ArrayList<DeleteImageForm>();
        String contextPath = servletContext.getRealPath("/pages/");
        if (new File(contextPath +"/images/albums/"+ id).exists())
        {
            String [] album = projectService.getAlbumForSite(id);
            for (int i = 0; i < album.length; i++)
                deleteImageForms.add(new DeleteImageForm(false, album[i]));
        }
            EditProjectForm editProjectForm = new EditProjectForm();
            editProjectForm.setDeleteImageForms(deleteImageForms);
        model.addAttribute("editProjectForm", editProjectForm);
        return "admin/editproject";
    }

    @RequestMapping(value ="/project/edit/{id}", method = RequestMethod.POST)
    public ModelAndView editProject(@PathVariable Integer id, @ModelAttribute("editProjectForm") EditProjectForm editProjectForm, @RequestParam(value="image", required=false)
    MultipartFile image, BindingResult result, Project p, @RequestParam(value="album")List<MultipartFile> album) {
        ModelAndView model = new ModelAndView();
        p.setTitle(editProjectForm.getTitle());
        p.setDescription(editProjectForm.getDescription());
        p.setVideo(editProjectForm.getVideo());
            String[] paths = projectService.getAlbum(p.getId());
        if (paths.length != 0) {
            for (int i = 0; i < editProjectForm.getDeleteImageForms().size(); i++) {
                if (editProjectForm.getDeleteImageForms().get(i).isSelected()) {
                    System.out.println(paths[i]);
                    new File(paths[i]).delete();
                }
            }
            }

            projectService.change(p, album);

        if (result.hasErrors()) {
            model.setViewName("redirect:/admin/project/edit" + id);
            return model;
        }
        if (!image.isEmpty()) {
            try {
                validateImage(image);
                saveImage(p.getId() + ".jpg", image);
            } catch (ImageUploadException e) {
                result.rejectValue("image","image.notvalid",e.getMessage());
                model.setViewName("/admin/editproject");
                return model;
            }
        }
        model.setViewName("redirect:/admin/project/edit/" + id);
        return model;
    }

    @RequestMapping(value ="/project/add", method = RequestMethod.GET)
    public ModelAndView showAddPage(ModelMap modelForm) {
        AddProjectForm addProjectForm = new AddProjectForm();
        modelForm.put("formAddProject", addProjectForm);
        ModelAndView model = new ModelAndView();
        model.setViewName("admin/createproject");
        return model;
    }

    @RequestMapping(value ="/company", method = RequestMethod.GET)
    public ModelAndView companyPage(ModelMap modelForm)
    {
        modelForm.put("changeCompanyForm", companyForm);
        ModelAndView model = new ModelAndView();
        model.setViewName("admin/editcompany");
        return model;
    }

    @RequestMapping(value ="/company", method = RequestMethod.POST)
    public ModelAndView editCompanyPage(@RequestParam(value="image", required=false) MultipartFile image,@Valid @ModelAttribute("changeCompanyForm") ChangeCompanyForm changeCompanyForm, BindingResult result) {
        ModelAndView model = new ModelAndView();
        if (result.hasErrors())
        {
            model.setViewName("admin/editcompany");
            result.rejectValue("text","company.size", "too small text");
            return model;
        }
        if (!image.isEmpty()) {
            try {
                validateImage(image);
                saveImage("company.jpg", image);
            } catch (ImageUploadException e) {
                accountService.editCompany(changeCompanyForm.getText());
                result.rejectValue("image","image.notvalid",e.getMessage());
                model.setViewName("admin/editcompany");
                return model;
            }
        }
        accountService.editCompany(changeCompanyForm.getText());
        logger.info(currentUser.getUsername() + " edited company page");
        model.setViewName("admin/editcompany");
        return model;
    }

    @RequestMapping(value ="/contacts", method = RequestMethod.GET)
    public ModelAndView contactsPage(ModelMap modelForm) {
        modelForm.put("changeContactsForm", contactsForm);
        ModelAndView model = new ModelAndView();
        model.setViewName("admin/editcontacts");
        return model;
    }

    @RequestMapping(value ="/contacts", method = RequestMethod.POST)
    public ModelAndView editContactsPage(@Valid @ModelAttribute("changeContactsForm") ChangeContactsForm changeContactsForm, BindingResult result) {
        ModelAndView model = new ModelAndView();
        if (result.hasErrors())
        {
            model.setViewName("admin/editcontacts");
            result.rejectValue("text","contacts.size", "too small text");
            return model;
        }
        accountService.editContacts(changeContactsForm.getText());
        logger.info(currentUser.getUsername()+" edited contacts page");
        model.setViewName("redirect:/admin/contacts");
        return model;
    }

    @RequestMapping(value ="/tickets", method = RequestMethod.GET)
    public ModelAndView ticketsPage(ModelMap modelForm) {
        modelForm.put("changeTicketsForm", ticketsForm);
        ModelAndView model = new ModelAndView();
        model.setViewName("admin/edittickets");
        return model;
    }

    @RequestMapping(value ="/tickets", method = RequestMethod.POST)
    public ModelAndView editContactsPage(@Valid @ModelAttribute("changeTicketsForm") ChangeTicketsForm changeTicketsForm, BindingResult result) {
        ModelAndView model = new ModelAndView();
        if (result.hasErrors())
        {
            model.setViewName("admin/edittickets");
            result.rejectValue("text","tickets.size", "too small text");
            return model;
        }
        accountService.editTickets(changeTicketsForm.getText());
        model.setViewName("redirect:/admin/tickets");
        return model;
    }

    @RequestMapping(value ="/project/add", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public ModelAndView processingAddProject(@Valid @ModelAttribute("formAddProject") AddProjectForm addProjectForm, BindingResult result, Project project, @RequestParam(value="image", required=true)
    MultipartFile image, @RequestParam(value="album")List<MultipartFile> album)
    {
        ModelAndView model = new ModelAndView();
        if (result.hasErrors()) {
            model.setViewName("admin/createproject");
            return model;
        }
        if (!image.isEmpty() || image != null) {
            try {
                validateImage(image);
                project.setTitle(addProjectForm.getTitle());
                project.setDescription(addProjectForm.getDescription());
                project.setVideo(addProjectForm.getVideo());
                saveImage(projectService.addProject(project.getTitle(), null, project.getVideo(), project.getDescription(), currentUser, album).getId() + ".jpg", image);
            } catch (ImageUploadException e) {
                result.rejectValue("image","image.notvalid",e.getMessage());
                model.setViewName("admin/createproject");
                return model;
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        } else {
            result.rejectValue("image", "image.notfound", "Image must be upload!!!");
        }
        model.setViewName("redirect:/admin/project/edit");
        return model;
    }

    private void validateImage(MultipartFile image) throws ImageUploadException {
        if (!image.getContentType().equals("image/jpeg") & !image.getContentType().equals("image/png")) {
            throw new ImageUploadException("Only JPG and PNG images accepted"); }
    }

    private void saveImage(String filename, MultipartFile image) {
        String contextPath = servletContext.getRealPath("/pages/");
        File file = new File(contextPath + "/images/" + filename);

        try {
            FileUtils.writeByteArrayToFile(file, image.getBytes());
        } catch (IOException e) {
            throw new ImageUploadException("Unable to save image", e);
        }

    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value ="/moderators/add", method = RequestMethod.GET)
    public ModelAndView showAddModerPage(ModelMap modelForm) {
        Account account = new Account();
        modelForm.put("formAddModerator", account);
        ModelAndView model = new ModelAndView();
        model.setViewName("admin/addmoderator");
        return model;
    }
    @Secured("ROLE_ADMIN")
    @RequestMapping(value ="/moderators/add", method = RequestMethod.POST)
    public ModelAndView processAddModerPage(@ModelAttribute("formAddModerator") Account account) {
        accountService.addAccount(account.getUsername(), account.getMail());
        ModelAndView model = new ModelAndView();
        model.setViewName("redirect:/admin/moderators/edit");
        return model;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value ="/moderators/edit", method = RequestMethod.GET)
    public ModelAndView showEditModPage() {
        ModelAndView model = new ModelAndView();
        model.addObject("lists", accountMap);
        model.setViewName("admin/listmoderators");
        return model;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value ="/moderators/delete/{username}", method = RequestMethod.GET)
    public ModelAndView deleteMod(@PathVariable String username) {
        ModelAndView model = new ModelAndView();
        Account account = accountService.getAccount(username);
        accountService.deleteAccount(account);
        model.setViewName("redirect:/admin/moderators/edit");
        return model;
    }

    @RequestMapping(value ="/profile", method = RequestMethod.GET)
    public ModelAndView showEditProfPage() {
        ModelAndView model = new ModelAndView();
        ChangeProffile changeProffile = new ChangeProffile();
        changeProffile.setUsername(currentUser.getUsername());
        changeProffile.setEmail(currentUser.getMail());
        model.addObject("editProfile", changeProffile);
        model.setViewName("admin/profile");
        return model;
    }

    @RequestMapping(value ="/profile", method = RequestMethod.POST)
    public ModelAndView processEditProfPage(@ModelAttribute("editProfile") ChangeProffile changeProffile) {
        ModelAndView model = new ModelAndView();
        currentUser.setUsername(changeProffile.getUsername());
        currentUser.setMail(changeProffile.getEmail());
        accountService.updateAccount(currentUser);
        model.setViewName("admin/profile");
        return model;
    }

    @RequestMapping(value ="/changepass", method = RequestMethod.GET)
    public ModelAndView changepass() {
        ModelAndView model = new ModelAndView();
        ChangePassword changePassword = new ChangePassword();
        model.addObject("changepass", changePassword);
        model.setViewName("admin/changepassword");
        return model;
    }

    @RequestMapping(value ="/changepass", method = RequestMethod.POST)
    public ModelAndView processChangePass(@ModelAttribute("changepass") ChangePassword changePassword, BindingResult bindingResult) {
        ModelAndView model = new ModelAndView();
        if (shaPasswordEncoder.encodePassword(changePassword.getOldPassword(), null).equals(currentUser.getPassword())) {
            if (changePassword.getNewPassword().equals(changePassword.getNewPassword2())) {
                accountService.changePassword(currentUser, changePassword.getNewPassword());
            } else {
                bindingResult.rejectValue("newPassword", "password.notfound", "Repeated password not correct");
                model.setViewName("admin/changepassword");
                return model;
            }
        } else {
            bindingResult.rejectValue("oldPassword", "password.notfound", "Old password not correct");
            model.setViewName("admin/changepassword");
            return model;
        }

        model.addObject("lists", accountService.getAllAccounts());
        model.setViewName("redirect:/admin/profile");
        return model;
    }

    @RequestMapping(value ="/partners/add", method = RequestMethod.GET)
    public ModelAndView addPartners() {
        ModelAndView model = new ModelAndView();
        AddPartner addPartner = new AddPartner();
        model.addObject("addPartner", addPartner);
        model.setViewName("admin/addpartners");
        return model;
    }

    @RequestMapping(value ="/partners/add", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public ModelAndView processingAddPartner(@Valid @ModelAttribute("addPartner") AddPartner addPartner, BindingResult result, @RequestParam(value="image", required=false)
    MultipartFile image){
        ModelAndView model = new ModelAndView();
        if (result.hasErrors()) {
            model.setViewName("admin/addpartners");
            return model;
        }
        if (!image.isEmpty() || image!=null) {
            try {
                validatePartnerImage(image);
                savePartnerImage(partnerService.add(addPartner.getTitle(), addPartner.getUrl()).getId() + ".jpg", image);
            } catch (ImageUploadException e) {
                result.rejectValue("image","image.notvalid",e.getMessage());
                model.setViewName("admin/addpartners");
                return model;
            }
        } else {
            result.rejectValue("image", "image.notfound", "Image must be upload!!!");
        }
        model.setViewName("redirect:/admin/partners/edit");
        return model;
    }

    private void validatePartnerImage(MultipartFile image) throws ImageUploadException {
        if (!image.getContentType().equals("image/jpeg") & !image.getContentType().equals("image/png")) {
            throw new ImageUploadException("Only JPG and PNG images accepted"); }
    }

    private void savePartnerImage(String filename, MultipartFile image) {
        String contextPath = servletContext.getRealPath("/pages/");
        File file = new File(contextPath + "/images/partner/" + filename);

        try {
            FileUtils.writeByteArrayToFile(file, image.getBytes());
        } catch (IOException e) {
            throw new ImageUploadException("Unable to save image", e);
        }

    }

    @RequestMapping(value ="/partners/edit", method = RequestMethod.GET)
    public ModelAndView editPartners() {
        ModelAndView model = new ModelAndView();
        model.addObject("partners", partnersMap);
        model.setViewName("admin/listpartners");
        return model;
    }

    @RequestMapping(value ="/partners/edit/{partner}", method = RequestMethod.GET)
    public String showEditPartners(@PathVariable Integer partner, Model model, ModelMap modelMap) {
        AddPartner addPartner = new AddPartner();
        modelMap.put("partnersForm", addPartner);
        Partner p = (Partner) partnersMap.get(partner);
        model.addAttribute("partner", p);
        return "admin/editpartner";
    }

    @RequestMapping(value ="/partners/edit/{partner}", method = RequestMethod.POST)
    public ModelAndView showEditPartners(@PathVariable Integer partner, @Valid @ModelAttribute("partnersForm") AddPartner addPartner, @RequestParam(value = "image", required = false) MultipartFile image, BindingResult result, Partner p) {
        ModelAndView model = new ModelAndView();
        p.setTitle(addPartner.getTitle());
        p.setUrl(addPartner.getUrl());
        p.setId(partner);
        partnerService.change(partner, p);
        if (result.hasErrors()) {
            model.setViewName("redirect:/admin/partners/edit/" + partner);
            return model;
        }
        if (!image.isEmpty()) {
            try {
                validatePartnerImage(image);
                savePartnerImage(p.getId() + ".jpg", image);
            } catch (ImageUploadException e) {
                result.rejectValue("image", "image.notvalid", e.getMessage());
                model.setViewName("/admin/editpartner");
                return model;
            }
        }
        model.setViewName("redirect:/admin/partners/edit/" + partner);
        return model;
    }

    @RequestMapping(value ="/partners/delete/{partner}", method = RequestMethod.GET)
    public ModelAndView deletePartner(@PathVariable int partner) {
        ModelAndView model = new ModelAndView();
        Partner partner1 = (Partner) partnersMap.get(partner);
        partnerService.remove(partner1);
        model.setViewName("redirect:/admin/partners/edit");
        return model;
    }

}
