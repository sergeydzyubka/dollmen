package dollmen.service;

import dollmen.DAO.ProjectDAO;
import dollmen.entities.Account;
import dollmen.entities.Project;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sergeydzyubka on 23.03.15.
 */
@Service
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class ProjectService
{
    static final Logger logger = Logger.getLogger(ProjectService.class);

    @Autowired
    private ProjectDAO projectDAO;

    @Autowired
    @Lazy
    private HashMap <Integer,Project> projectMap;

    @Autowired
    @Lazy
    private Account currentUser;

    @Autowired
    @Lazy
    private ArrayList projectsList;

    @Autowired
    private ServletContext servletContext;


    public Project addProject (String title, String photos, String video, String desc,Account author, List<MultipartFile> album) throws IOException {
        String contextPath = servletContext.getRealPath("/pages");

        Project project = new Project();
        project.setTitle(title);
        project.setPhotos(photos);
        project.setVideo(video);
        project.setAuthor(author);
        project.setDescription(desc);
        projectDAO.add(project);
        projectMap.put(project.getId(), project);
        projectsList.add(project);
        new File(contextPath+"/images/albums/" + project.getId()).mkdir();
        addPhotos(project.getId(), album);
        return project;
    }

    private void addPhotos(Integer id,List<MultipartFile> photos) {
        String contextPath = servletContext.getRealPath("/pages/");

        for (MultipartFile file : photos)
        {
            try {
                FileUtils.writeByteArrayToFile(new File(contextPath+"/images/albums/"+id+"/"+file.getOriginalFilename()), file.getBytes());
            } catch (IOException e) {
            }
        }
    }

    public void change(Project project, List<MultipartFile> album)
    {
        project.setAuthor(currentUser);
        projectsList.remove(projectMap.get(project.getId()));
        projectMap.put(project.getId(), project);
        projectsList.add(project);
        addPhotos(project.getId(), album);
        projectDAO.update(projectMap.get(project.getId()));
        logger.info("Project '" +project.getTitle() + "'edited by "+currentUser.getUsername());
    }

    public HashMap<Integer,Project> loadAll()
    {
        HashMap <Integer,Project> map = new HashMap<Integer,Project>();
        for(Project project: projectDAO.getAllProjects()) {
            map.put(project.getId(), project);
        }
        return map;
    }

    public ArrayList<Project> getProjectsList()
    {
        return new ArrayList(projectDAO.getAllProjects());
    }

    public void remove (Project project)
    {
        projectMap.remove(project.getId());
        projectsList.remove(project);
        removeDir(project.getId());
        projectDAO.delete(project);
        logger.info("Project '"+project.getTitle()+"' removed by "+project.getAuthor().getUsername());
    }

    public String[] getAlbum(Integer id)
    {
        String contextPath = servletContext.getRealPath("/pages/");
        File [] files = new File(contextPath + "/images/albums/"+id).listFiles();
        if (files == null) {
            return new String[0];
        }
        String[] paths = new String[files.length];
        for (int i=0;i<files.length;i++) {
            if (files[i].getName().equals(".DS_Store")) {
                i++;
            }
                paths[i] = contextPath+"/images/albums/" + id + "/" + files[i].getName();
        }
        return paths;
    }

    public String[] getAlbumForSite(Integer id)
    {
        String contextPath = servletContext.getRealPath("/pages/");
        File [] files = new File(contextPath + "/images/albums/"+id).listFiles();
        if (files == null) {
            return new String[0];
        }
        String[] paths = new String[files.length];
        for (int i=0;i<files.length;i++) {
            if (files[i].getName().equals(".DS_Store") || !files[i].getName().contains(".")) {
                i++;
            }
            paths[i] = "/images/albums/" + id + "/" + files[i].getName();
        }
        return paths;
    }

    private void removeDir (Integer id) {
            String contextPath = servletContext.getRealPath("/pages/");
            File[] files = new File(contextPath + "/images/albums/" + id).listFiles();
        if (new File(contextPath + "/images/albums/" + id).exists()) {
            for (File file : files)
                file.delete();
            new File(contextPath + "/images/albums/" + id).delete();
        }
    }
}
