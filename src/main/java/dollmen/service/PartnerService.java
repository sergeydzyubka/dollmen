package dollmen.service;

import dollmen.DAO.PartnerDAO;
import dollmen.entities.Account;
import dollmen.entities.Partner;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by sergeydzyubka on 10.04.15.
 */
@Service
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class PartnerService
{
    static final Logger logger = Logger.getLogger(PartnerService.class);

    @Autowired
    private PartnerDAO partnerDAO;

    @Autowired
    @Lazy
    private HashMap<Integer,Partner> partnersMap;

    @Autowired
    @Lazy
    private Account currentUser;

    public HashMap<Integer,Partner>loadAll()
    {
        HashMap <Integer,Partner> map = new HashMap<Integer,Partner>();
        for(Partner partner: partnerDAO.getAllPartners()) {
            map.put(partner.getId(), partner);
        }
        return map;
    }

    public void change(Integer old, Partner partner)
    {
        partnersMap.remove(old);
        partnersMap.put(partner.getId(),partner);
        partnerDAO.update(partnersMap.get(partner.getId()));
        logger.info(currentUser.getUsername()+" edited partner with title '"+partner.getTitle()+"'");
    }

    public Partner add (String title, String url)
    {
        Partner partner = new Partner(title,url);
        partnerDAO.add(partner);
        partnersMap.put(partner.getId(),partner);
        logger.info(currentUser.getUsername()+" added partner with title '"+title+"'");
        return partner;
    }

    public void remove (Partner partner)
    {
        partnersMap.remove(partner.getId());
        partnerDAO.delete(partner);
        logger.info(currentUser.getUsername()+" removed partner with title '"+partner.getTitle()+"'");
    }
}
