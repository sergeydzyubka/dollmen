package dollmen.service;

import dollmen.DAO.AccountDAO;
import dollmen.entities.Account;
import dollmen.entities.Authority;
import dollmen.entities.Role;
import dollmen.forms.ChangeCompanyForm;
import dollmen.forms.ChangeContactsForm;
import dollmen.forms.ChangeTicketsForm;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by sergeydzyubka on 28.03.15.
 */
@Service
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class AccountService implements UserDetailsService {
    static final Logger logger = Logger.getLogger(AccountService.class);

    @Autowired
    private AccountDAO accountDAO;

    @Autowired
    private ShaPasswordEncoder shaPasswordEncoder;

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private Session mailSession;

    @Autowired
    @Lazy
    private HashMap<String, Account> accountMap;

    @Autowired
    @Lazy
    private ChangeContactsForm changeContactsForm;

    @Autowired
    @Lazy
    private ChangeCompanyForm changeCompanyForm;

    @Autowired
    @Lazy
    private ChangeTicketsForm changeTicketsForm;

    @Autowired
    @Lazy
    private Account currentUser;

    public void editCompany(String text) {
        changeCompanyForm.setText(text);
        String contextPath = servletContext.getRealPath("/pages/");
        File file = new File(contextPath + "/text/company.txt");
        file.setWritable(true);
        try {
            FileUtils.write(file, text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void editContacts(String text) {
        changeContactsForm.setText(text);
        String contextPath = servletContext.getRealPath("/pages/");
        File file = new File(contextPath + "/text/contacts.txt");
        file.setWritable(true);
        try {
            FileUtils.write(file, text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void editTickets(String text) {
        changeTicketsForm.setText(text);
        String contextPath = servletContext.getRealPath("/pages/");
        File file = new File(contextPath + "/text/tickets.txt");
        file.setWritable(true);
        try {
            FileUtils.write(file, text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addDownload(String filename, MultipartFile download)
    {
        try {
            String contextPath = servletContext.getRealPath("/pages/");
            File file = new File(contextPath + "/downloads/" + filename);
            FileUtils.writeByteArrayToFile(file, download.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteDownload (String name)
    {
        String contextPath = servletContext.getRealPath("/pages/");
        File file = new File(contextPath + "/downloads/"+name);
        System.out.println(file.getAbsolutePath());
        System.out.println(file.delete());
    }

    public void addAccount(String username, String mail) {
        final Account account = new Account(username, generatePassword(8), mail);
        sendRegMsg(account);
        account.setPassword(shaPasswordEncoder.encodePassword(account.getPassword(), null));
        account.getAuthorities().add(new Authority(Role.ROLE_MODERATOR.toString(), account));
        accountDAO.add(account);
        accountMap.put(account.getUsername(), account);
        logger.info(currentUser.getUsername()+" added moderator with login '"+username+"' and e-mail '"+mail+"'");
    }

    private void sendRegMsg(Account account) {
        MimeMessage message = new MimeMessage(mailSession);
        try {
            message.setFrom(new InternetAddress(mailSession.getProperty("mail.smtp.user")));

            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(account.getMail()));

            message.setSubject("Dollmen moderator account");
            message.setText("Your login: " + account.getUsername() + "\nYour password: " + account.getPassword());
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void changePassword(Account account, String password) {
        account.setPassword(shaPasswordEncoder.encodePassword(password, null));
        accountDAO.update(account);
    }

    public Account getAccount(String name) {
        return accountDAO.getByName(name);
    }

    public UserDetails loadUserByUsername(String name) {
        Account account = accountDAO.getByName(name);
        List authList = account.getAuthorities();
        return new org.springframework.security.core.userdetails.User(account.getUsername(), account.getPassword(), true, true, true, true, authList);
    }

    public void updateAccount(Account account) {
        accountDAO.update(account);
    }

    public void deleteAccount(Account account) {
        if (!currentUser.getUsername().equals(account.getUsername())) {
            accountDAO.delete(account);
            accountMap.remove(account.getUsername());
            logger.info(currentUser.getUsername() + " removed moderator '" + account.getUsername()+"'");
        }
    }

    private String generatePassword(int length) {
        Random random = new Random((new Date()).getTime());
        char[] values = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3',
                '4', '5', '6', '7', '8', '9'};
        String out = "";
        for (int i = 0; i < length; i++) {
            int idx = random.nextInt(values.length);
            out += values[idx];
        }
        return out;
    }

    public HashMap<String, Account> getAllAccounts() {
        HashMap<String, Account> map = new HashMap<String, Account>();
        for (Account account : accountDAO.getAllAccounts()) {
            map.put(account.getUsername(), account);
        }
        return map;
    }

}
