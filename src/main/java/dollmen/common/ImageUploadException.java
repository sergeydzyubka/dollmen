package dollmen.common;

public class ImageUploadException extends RuntimeException {

    public ImageUploadException(String errorMessage) {
        super(errorMessage);
    }

    public ImageUploadException(String errorMessage, Exception e) {
        super(errorMessage, e);
    }

    public ImageUploadException(Throwable cause){
        super(cause);
    }

    public ImageUploadException(String message, Throwable cause){
        super(message, cause);
    }
}
