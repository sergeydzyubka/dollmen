package dollmen.config;

import dollmen.entities.Account;
import dollmen.entities.Partner;
import dollmen.entities.Project;
import dollmen.service.AccountService;
import dollmen.service.PartnerService;
import dollmen.service.ProjectService;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Properties;

@org.springframework.context.annotation.Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan (basePackages = {"dollmen"})
public class AppConfig extends WebMvcConfigurerAdapter
{
    @Autowired
    private PartnerService partnerService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private AccountService accountService;

    @Bean
    @Scope(BeanDefinition.SCOPE_SINGLETON)
    public ArrayList<Project>projectsList ()
    {
        return new ArrayList<Project>(projectService.getProjectsList())
        {
            @Override
            public boolean add (Project project)
            {
                if (!super.add(project))
                    return false;
                Collections.sort(this);
                return true;
            }
        };
    }


    @Bean
    public Session mailSession ()
    {
        final Properties props = new Properties();
        props.setProperty("mail.smtp.user", "dollmenua@gmail.com");
        props.setProperty("mail.smtp.password", "dollmen1234");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(props.getProperty("mail.smtp.user"),props.getProperty("mail.smtp.password"));
            }
        });
        session.setDebug(true);
        return session;
    }

    @Bean
    @Scope("session")
    public Account currentUser()
    {
        org.springframework.security.core.userdetails.User account = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return accountService.getAccount(account.getUsername());
    }

    @Bean(name = "dataSource")
    @Autowired
    public DataSource dataSource() {
        DataSource dataSource = new DataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost/dollmen");
        dataSource.setMaxAge(30000);
        dataSource.setNumTestsPerEvictionRun(50);
        dataSource.setMinEvictableIdleTimeMillis(30000);

        dataSource.setUsername("root");
        dataSource.setPassword("");
        dataSource.setMaxActive(10);
        dataSource.setMaxIdle(3);
        dataSource.setMinIdle(2);
        return dataSource;
    }


    @Bean
    public HashMap<Integer,Partner> partnersMap()
    {
        return partnerService.loadAll();
    }
    @Bean
    public HashMap<Integer, Project> projectMap ()
    {
        return projectService.loadAll();
    }

    @Bean
    public HashMap<String, Account> accountMap () {
        return accountService.getAllAccounts();
    }

    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.put("current_session_context_class", "org.hibernate.dialect.MySQL5Dialect");
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        return properties;
    }

//    @Bean
//    @Autowired
//    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory)
//    {
//        HibernateTransactionManager htm = new HibernateTransactionManager();
//        htm.setSessionFactory(sessionFactory);
//        return htm;
//    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory s) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(s);
        return txManager;
    }

//    @Bean(name = "sessionFactory")
//    @Autowired
//    public SessionFactory getSessionFactory(DataSource dataSource) {
//        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
//        sessionBuilder.addAnnotatedClasses(Project.class, Account.class, Authority.class, Partner.class);
//        sessionBuilder.addProperties(getHibernateProperties());
//        return sessionBuilder.buildSessionFactory();
//    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] { "dollmen.entities" });
        sessionFactory.setHibernateProperties(getHibernateProperties());
        return sessionFactory;
    }

//    @Bean
//    public SessionFactory sessionFactory() {
//        return new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
//    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("/pages/");
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/admin").setViewName("/admin/admin");
    }

    @Bean
    public UrlBasedViewResolver setupViewResolver() {
        UrlBasedViewResolver resolver = new UrlBasedViewResolver();
        resolver.setPrefix("/pages/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);

        return resolver;
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() throws MaxUploadSizeExceededException
    {
        CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
        commonsMultipartResolver.setDefaultEncoding("utf-8");
        //commonsMultipartResolver.setMaxUploadSize(52428800);
        return commonsMultipartResolver;
    }
}
