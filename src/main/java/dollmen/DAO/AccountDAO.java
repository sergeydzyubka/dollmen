package dollmen.DAO;

import dollmen.entities.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sergeydzyubka on 29.03.15.
 */
@Repository
@Transactional
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class AccountDAO
{
    @Autowired
    SessionFactory sessionFactory;

    private Session getSession(){
        Session session = sessionFactory.getCurrentSession();
        return session;
    }

    @Transactional
    public void add(Account user)
    {
        getSession().save(user);
    }

    @Transactional(readOnly = true)
    public List<Account> getAllAccounts ()
    {
        return getSession().createQuery("FROM " + Account.class.getName() + " ORDER BY id desc").list();
    }

    @Transactional(readOnly = true)
    public Account getByName(String username) {
        return (Account) getSession().createQuery("FROM " + Account.class.getName() + " WHERE username = :username").setParameter("username", username).uniqueResult();
    }


    public void update(Account u)
    {
        Account user = null;
            user = (Account) getSession().get(Account.class, u.getId());
            user.setUsername(u.getUsername());
            user.setPassword(u.getPassword());
            user.setMail(u.getMail());
            getSession().update(user);
    }

    @Transactional
    public void delete(Account u)
    {
        Account user = null;
            user = (Account) getSession().get(Account.class, u.getId());
            getSession().delete(user);
    }
}
