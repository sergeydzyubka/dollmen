package dollmen.DAO;

import dollmen.entities.Partner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sergeydzyubka on 10.04.15.
 */
@Repository
@Transactional
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class PartnerDAO
{
    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession(){
        Session session = sessionFactory.getCurrentSession();
        return session;
    }

    @Transactional
    public void update (Partner partner)
    {
        getSession().update(partner);
    }
    @Transactional(readOnly = true)
    public List<Partner> getAllPartners ()
    {
        return getSession().createQuery("FROM " + Partner.class.getName() + " ORDER BY id desc").list();
    }

    @Transactional
    public void add (Partner partner)
    {
        getSession().save(partner);
    }


    @Transactional
    public void delete (Partner partner) {
        getSession().delete(partner);
    }
}
