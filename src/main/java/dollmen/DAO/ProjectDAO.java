package dollmen.DAO;

import dollmen.entities.Account;
import dollmen.entities.Project;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sergeydzyubka on 21.02.15.
 */
@Repository
@Transactional
public class ProjectDAO
{

    public ProjectDAO() {

    }

    private static final int LIMIT_RESULTS_PER_PAGE = 5;

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private AccountDAO accountDAO;

    private Session getSession(){
        Session session = sessionFactory.getCurrentSession();
        return session;
    }

    public void update (Project project) {
        Account account = accountDAO.getByName(project.getAuthor().getUsername());
        project.setAuthor(account);
        getSession().saveOrUpdate(project);
    }

    @Transactional(readOnly = true)
    public List <Project> getAllProjects () {
        return getSession().createQuery("FROM " + Project.class.getName() + " ORDER BY id desc").list();
    }

    @Transactional(readOnly = true)
    public List<Project> getProjectsPerPage(int page) {
        return getSession().createQuery("FROM " + Project.class.getName() + " ORDER BY id desc").setFirstResult(page * LIMIT_RESULTS_PER_PAGE - LIMIT_RESULTS_PER_PAGE).setMaxResults(LIMIT_RESULTS_PER_PAGE).list();
    }

    @Transactional
    public void add (Project project) {
        Account account = accountDAO.getByName(project.getAuthor().getUsername());
        project.setAuthor(account);
        getSession().persist(project);
    }

    @Transactional
    public void delete (Project project)
    {
        getSession().delete(project);
    }
}
