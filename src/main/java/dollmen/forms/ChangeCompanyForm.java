package dollmen.forms;

/**
 * Created by sergeydzyubka on 13.04.15.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.validation.constraints.Size;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by sergeydzyubka on 11.04.15.
 */
@Component
public class ChangeCompanyForm
{
    @Autowired
    private ServletContext servletContext;

    public String getText() {
        if (text == null || text.equals(""))
            setText(read());
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public String read() {
        StringBuilder result = new StringBuilder("");
        String contextPath = servletContext.getRealPath("/pages/");
        File file = new File(contextPath + "/text/company.txt");
        try (Scanner scanner = new Scanner(file)) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    @Size(min=20)
    private String text;

    private MultipartFile image;

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }
}
