package dollmen.forms;

/**
 * Created by sergeydzyubka on 26.04.15.
 */
public class DeleteImageForm
{
    private boolean selected;
    private String path;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public DeleteImageForm(boolean selected, String path) {
        this.selected = selected;
        this.path = path;
    }

    public DeleteImageForm() {
    }

    public String getPath() {
        return path;
    }
}
