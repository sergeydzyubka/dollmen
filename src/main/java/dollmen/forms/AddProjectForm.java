package dollmen.forms;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;
import java.util.List;

public class AddProjectForm {

    private String video;
    private MultipartFile image;
    @Size(min=3, max=35)
    private String title;
    @Size(min=20)
    private String description;
    private List<MultipartFile> album;

    public List<MultipartFile> getAlbum() {
        return album;
    }

    public void setAlbum(List<MultipartFile> album) {
        this.album = album;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
}
