package dollmen.forms;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by sergeydzyubka on 18.04.15.
 */
public class AddDownload {

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
